# Projet colloque
Par Mathieu Margollé, TSI ENSG 2018-2019

Ce mini-projet a pour objectif de présenter 
l'utilisation basique du framework Spring Boot 
associé à la librairie Hibernate de persistence des données,
pour créer une application simple de gestion d'événements et leurs participants.

## Pré-requis

Le projet nécessite un environnement de développement Java 8, 
l'outil de gestion de projet Maven,
et une base de données vierge PostgreSQL.

#### Java 8 JDK

Pour installer le JDK de Java 8 sur une machine Ubuntu, exécuter les commandes suivantes :

    sudo add-apt-repository ppa:webupd8team/java
    sudo apt update
    sudo apt install oracle-java8-installer
    sudo apt install oracle-java8-set-default
    
#### Maven

Pour installer Maven sur une machine Ubuntu, télécharger l'archive .tar.gz 
de la dernière version sur le site https://maven.apache.org/download.cgi.

Décompresser l'archive :

    tar xzvf apache-maven-3.6.0-bin.tar.gz
    
Ajouter le chemin vers l'exécutable Maven à la variable Path :

    export PATH=/votre/chemin/apache-maven-3.6.0/bin:$PATH

#### Base de données PostgreSQL

Pour installer PostgreSQL sur une machine Ubuntu, exécuter les commandes suivantes :

    sudo apt update
    sudo apt install postgresql postgresql-contrib

Créer un utilisateur (dans notre cas, _mmargolle_) :

    sudo -u postgres createuser --createdb --pwprompt mmargolle

Renseigner le mot de passe du nouvel utilisateur quand demandé 
(dans notre cas, _mmargolle_ à nouveau).

Créer ensuite une base de données avec cet utilisateur 
(appelée dans notre cas _colloque_mmargolle_) :

    sudo -u mmargolle createdb colloque_mmargolle
    
Les informations de connexion à la base de données sont contenues
dans le fichier `src/main/resources/application.properties`, 
où vous pouvez les modifier si vous avez utilisé des valeurs différentes :

```yaml
spring.datasource.driver-class-name=org.postgresql.Driver
spring.datasource.url=jdbc:postgresql://localhost:5432/colloque_mmargolle
spring.datasource.username=mmargolle
spring.datasource.password=mmargolle
spring.jpa.show-sql=true
spring.jpa.hibernate.ddl-auto=create
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.PostgreSQL95Dialect
spring.jpa.properties.hibernate.jdbc.lob.non_contextual_creation=true
```

## Utilisation

Lancer l'application avec la commande Maven suivante à la racine du projet :

    mvn spring-boot:run
    
Alternativement, compiler et exécuter un Jar avec les commandes suivantes :

    mvn clean package
    java -jar target/colloque-1.0-SNAPSHOT.jar
    
Accéder enfin à l'application à l'adresse suivante : http://localhost:8080/

    