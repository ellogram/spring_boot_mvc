package eu.ensg.mmargolle.colloque.model;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Participant {

    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy="increment")
    private Long id;

    @NotBlank
    private String nom;

    @NotBlank
    private String prenom;

    @Email
    @NotBlank
    private String email;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Past
    @NotNull
    private Date dateNaiss;

    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "participants")
    private List<Evenement> evenements;

    public Participant() {
    }

    public Participant(String nom, String prenom, String email, String dateNaiss) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            this.dateNaiss = df.parse(dateNaiss);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.evenements = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDateNaiss() {
        return dateNaiss;
    }

    public void setDateNaiss(Date dateNaiss) {
        this.dateNaiss = dateNaiss;
    }

    public List<Evenement> getEvenements() {
        return evenements;
    }

    public void setEvenements(List<Evenement> evenements) {
        this.evenements = evenements;
    }

    public void addEvenement(Evenement evenement) {
        this.evenements.add(evenement);
        if (!evenement.getParticipants().contains(this))
            evenement.addParticipant(this);
    }

    public void removeEvenement(Evenement evenement) {
        this.evenements.remove(evenement);
        if (evenement.getParticipants().contains(this))
            evenement.removeParticipant(this);
    }

    @PreRemove
    private void removeParticipantFromEvenements() {
        for (Evenement e : evenements) {
            e.getParticipants().remove(this);
        }
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "-" + getId();
    }
}
