package eu.ensg.mmargolle.colloque.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Evenement {

    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy="increment")
    private Long id;

    @NotBlank
    private String intitule;

    @NotBlank
    private String theme;

    @Temporal(TemporalType.DATE)
    @NotNull
    private Date dateDebut;

    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<Participant> participants;

    public Evenement() {
    }

    public Evenement(String intitule, String theme, String dateDebut) {
        this.intitule = intitule;
        this.theme = theme;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            this.dateDebut = df.parse(dateDebut);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.participants = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public List<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }

    public void addParticipant(Participant participant) {
        this.participants.add(participant);
        if (!participant.getEvenements().contains(this))
            participant.addEvenement(this);
    }

    public void removeParticipant(Participant participant) {
        this.participants.remove(participant);
        if (participant.getEvenements().contains(this))
            participant.removeEvenement(this);
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "-" + getId();
    }

}
