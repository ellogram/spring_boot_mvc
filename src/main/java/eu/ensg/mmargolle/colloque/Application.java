package eu.ensg.mmargolle.colloque;

import eu.ensg.mmargolle.colloque.model.Evenement;
import eu.ensg.mmargolle.colloque.model.Participant;
import eu.ensg.mmargolle.colloque.repository.EvenementRepository;
import eu.ensg.mmargolle.colloque.repository.ParticipantRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class Application {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public CommandLineRunner demo(EvenementRepository repository, ParticipantRepository repository2) {
        return (args) -> {
            // save a couple of evenements
            Evenement picasso = new Evenement("Exposition Picasso", "Grands peintres", "2019-02-22");
            Evenement retro = new Evenement("Rétrospective Rossini", "Grands compositeurs", "2019-02-21");
            Participant toto = new Participant("Dupont","Bernard", "toto@gmail.com", "2000-12-12");
//            toto.addEvenement(picasso);
            toto.addEvenement(retro);
            repository.save(picasso);
            repository.save(retro);
            Participant tata = new Participant("Dufour","Marcel", "tata@gmail.com", "2001-01-01");
            repository2.save(tata);

            // fetch all evenements
            log.info("Evenements found with findAll():");
            log.info("-------------------------------");
            for (Evenement evenement : repository.findAll()) {
                log.info(evenement.toString());
            }
            log.info("");

            // fetch all participants
            log.info("Participants found with findAll():");
            log.info("-------------------------------");
            for (Participant participant : repository2.findAll()) {
                log.info(participant.toString());
            }
            log.info("");

            // fetch an individual evenement by ID
            repository.findById(1L)
                .ifPresent(evenement -> {
                    log.info("Evenement found with findById(1L):");
                    log.info("--------------------------------");
                    log.info(evenement.toString());
                    log.info("");
                });

            // fetch evenements by participant
            log.info("Evenements found with findByParticipantsContains(toto):");
            log.info("--------------------------------------------");
            for (Evenement evenement : repository.findByParticipantsContains(toto)) {
                log.info(evenement.toString());
            }
            log.info("");

            // fetch participants by evenement
            log.info("Participants found with findByEvenementsContains(retro):");
            log.info("--------------------------------------------");
            for (Participant participant : repository2.findByEvenementsContains(retro)) {
                log.info(participant.toString());
            }
            log.info("");
        };
    }
}
