package eu.ensg.mmargolle.colloque.controller;

import eu.ensg.mmargolle.colloque.model.Participant;
import eu.ensg.mmargolle.colloque.repository.EvenementRepository;
import eu.ensg.mmargolle.colloque.repository.ParticipantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping(path="/participant")
public class ParticipantCreateController {

    private final ParticipantRepository participantRepository;
    private final EvenementRepository evenementRepository;

    @Autowired
    public ParticipantCreateController(ParticipantRepository participantRepository, EvenementRepository evenementRepository) {
        this.participantRepository = participantRepository;
        this.evenementRepository = evenementRepository;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(
                Date.class,
                new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true, 10)
        );
    }

    @GetMapping("/create")
    public String create(Model model) {
        if (!model.containsAttribute("participant")) {
            model.addAttribute("participant", new Participant());
        }
        model.addAttribute("allEvenements", evenementRepository.findAll());
        model.addAttribute("action", "create");
        model.addAttribute("page_title", "Créer un participant");
        return "participant/createOrEdit";
    }

    @PostMapping("/create")
    public String save(@Valid Participant participant, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("participant", participant);
            return create(model);
        }
        participantRepository.save(participant);
        return "redirect:/participant/all";
    }

}
