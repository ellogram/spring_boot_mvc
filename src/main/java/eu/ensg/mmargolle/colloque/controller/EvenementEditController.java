package eu.ensg.mmargolle.colloque.controller;

import eu.ensg.mmargolle.colloque.model.Evenement;
import eu.ensg.mmargolle.colloque.repository.EvenementRepository;
import eu.ensg.mmargolle.colloque.repository.ParticipantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping(path="/evenement")
public class EvenementEditController {

    private final EvenementRepository evenementRepository;
    private final ParticipantRepository participantRepository;

    @Autowired
    public EvenementEditController(EvenementRepository evenementRepository, ParticipantRepository participantRepository) {
        this.evenementRepository = evenementRepository;
        this.participantRepository = participantRepository;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(
                Date.class,
                new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true, 10)
        );
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") long id, Model model) {
        if (!model.containsAttribute("evenement")) {
            Evenement evenement = evenementRepository.findById(id)
                    .orElseThrow(() -> new IllegalArgumentException("Evenement introuvable : " + id));
            model.addAttribute("evenement", evenement);
        }
        model.addAttribute("allParticipants", participantRepository.findAll());
        model.addAttribute("action", "edit/" + id);
        model.addAttribute("page_title", "Modifier l'événement n°" + id);
        return "evenement/createOrEdit";
    }

    @PostMapping("/edit/{id}")
    public String update(@PathVariable("id") long id, @Valid Evenement evenement, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("evenement", evenement);
            return edit(id, model);
        }
        evenementRepository.save(evenement);
        return "redirect:/evenement/all";
    }
}
