package eu.ensg.mmargolle.colloque.controller;

import eu.ensg.mmargolle.colloque.repository.ParticipantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path="/participant")
public class ParticipantShowController {

    private final ParticipantRepository participantRepository;

    @Autowired
    public ParticipantShowController(ParticipantRepository participantRepository) {
        this.participantRepository = participantRepository;
    }

    @GetMapping(value = {"/all", ""})
    public String showAll(Model model) {
        model.addAttribute("participants", participantRepository.findAll());
        return "participant/showAll";
    }

}
