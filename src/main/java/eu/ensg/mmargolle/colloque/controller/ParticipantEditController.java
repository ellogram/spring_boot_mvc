package eu.ensg.mmargolle.colloque.controller;

import eu.ensg.mmargolle.colloque.model.Participant;
import eu.ensg.mmargolle.colloque.repository.EvenementRepository;
import eu.ensg.mmargolle.colloque.repository.ParticipantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping(path="/participant")
public class ParticipantEditController {

    private final ParticipantRepository participantRepository;
    private final EvenementRepository evenementRepository;

    @Autowired
    public ParticipantEditController(ParticipantRepository participantRepository, EvenementRepository evenementRepository) {
        this.participantRepository = participantRepository;
        this.evenementRepository = evenementRepository;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(
                Date.class,
                new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true, 10)
        );
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") long id, Model model) {
        if (!model.containsAttribute("participant")) {
            Participant participant = participantRepository.findById(id)
                    .orElseThrow(() -> new IllegalArgumentException("Participant introuvable : " + id));
            model.addAttribute("participant", participant);
        }
        model.addAttribute("allEvenements", evenementRepository.findAll());
        model.addAttribute("action", "edit/" + id);
        model.addAttribute("page_title", "Modifier le participant n°" + id);
        return "participant/createOrEdit";
    }

    @PostMapping("/edit/{id}")
    public String update(@PathVariable("id") long id, @Valid Participant participant, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("participant", participant);
            return edit(id, model);
        }
        participantRepository.save(participant);
        return "redirect:/participant/all";
    }

}
