package eu.ensg.mmargolle.colloque.controller;

import eu.ensg.mmargolle.colloque.repository.EvenementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path="/evenement")
public class EvenementShowController {

    private final EvenementRepository evenementRepository;

    @Autowired
    public EvenementShowController(EvenementRepository evenementRepository) {
        this.evenementRepository = evenementRepository;
    }

    @GetMapping(value = {"/all", ""})
    public String showAll(Model model) {
        model.addAttribute("evenements", evenementRepository.findAll());
        return "evenement/showAll";
    }

}
