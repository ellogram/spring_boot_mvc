package eu.ensg.mmargolle.colloque.controller;

import eu.ensg.mmargolle.colloque.model.Participant;
import eu.ensg.mmargolle.colloque.repository.ParticipantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path="/participant")
public class ParticipantDeleteController {

    private final ParticipantRepository participantRepository;

    @Autowired
    public ParticipantDeleteController(ParticipantRepository participantRepository) {
        this.participantRepository = participantRepository;
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id, Model model) {
        Participant participant = participantRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Participant introuvable : " + id));
        participantRepository.delete(participant);
        return "redirect:/participant/all";
    }
}
