package eu.ensg.mmargolle.colloque.controller;

import eu.ensg.mmargolle.colloque.model.Evenement;
import eu.ensg.mmargolle.colloque.repository.EvenementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path="/evenement")
public class EvenementDeleteController {

    private final EvenementRepository evenementRepository;

    @Autowired
    public EvenementDeleteController(EvenementRepository evenementRepository) {
        this.evenementRepository = evenementRepository;
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id, Model model) {
        Evenement evenement = evenementRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Evenement introuvable : " + id));
        evenementRepository.delete(evenement);
        return "redirect:/evenement/all";
    }
}
