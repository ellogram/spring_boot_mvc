package eu.ensg.mmargolle.colloque.repository;

import eu.ensg.mmargolle.colloque.model.Participant;
import eu.ensg.mmargolle.colloque.model.Evenement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ParticipantRepository extends JpaRepository<Participant, Long> {

    List<Participant> findByEvenementsContains(Evenement evenement);

}