package eu.ensg.mmargolle.colloque.repository;

import eu.ensg.mmargolle.colloque.model.Evenement;
import eu.ensg.mmargolle.colloque.model.Participant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface EvenementRepository extends JpaRepository<Evenement, Long> {

    List<Evenement> findByParticipantsContains(Participant participant);

}